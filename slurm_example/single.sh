#!/bin/bash

#SBATCH --cpus-per-task=8 --chdir=/vol/spool/

# A very simple script for downloading fast files via ganesha store and analyse them directly via kraken2
# Usage: sbatch /path/to/this/script.sh [path_to_fastq_files]
# WARNING: Make sure to pass an ABSOLUTE path to this script, or use $(pwd) as input when in target fastq directory.
# Example: sbatch ~/gcb2019-ansible/slurm_example/single.sh <path_to_fastq_files>

# Prepare Job folder
folder_date=$(date +%Y%m%d_%H%M%S)
mkdir -p /vol/scratch/$folder_date

# Count fastq parts
count=ls $(realpath $1) | wc -l

# Copy fastq files from ganesha to work folder
cp $(realpath $1)/* /vol/scratch/$folder_date/
mkdir -p /vol/spool/$folder_date/

# Do we have paired fastq files or not?
if [[ $count -eq 2 ]]
then
    /usr/local/bin/kraken2 --db /vol/scratch/kraken2db/ --paired --threads 28 /vol/scratch/$folder_date/*.fastq.gz > /vol/spool/$folder_date/result.txt
else
    /usr/local/bin/kraken2 --db /vol/scratch/kraken2db/ --threads 28 /vol/scratch/$folder_date/*.fastq.gz > /vol/spool/$folder_date/result.txt
fi

# Format result, so that krona can use it
cat /vol/spool/$folder_date/result.txt | cut -f 2,3 > /vol/spool/$folder_date/result.krona

rm /vol/spool/$folder_date/result.txt
rm -rf  /vol/scratch/$folder_date