#!/bin/bash

#SBATCH --array=0-3 -n 1 --spread-job --chdir=/vol/spool/ --cpus-per-task=28

# Download a bunch of fastq on an array at the same time and analyse them with kraken2

echo I have been calculated on $(hostname)

number=${SLURM_ARRAY_TASK_ID}
folder_date=$(date +%Y%m%d_%H%M%S)

mkdir -p /vol/scratch/$folder_date/$number/
mkdir -p /vol/spool/krona_results/

# Depending on the provisioned array index by the SLURM scheduler, target one of these sets.
urls=(
    'bielefeld/ftp.era.ebi.ac.uk/vol1/fastq/SRR592/009/SRR5925339/SRR5925339_1.fastq.gz'
    'bielefeld/ftp.era.ebi.ac.uk/vol1/fastq/SRR592/009/SRR5925339/SRR5925339_2.fastq.gz'
    'bielefeld/ftp.era.ebi.ac.uk/vol1/fastq/SRR592/008/SRR5925338/SRR5925338_1.fastq.gz'
    'bielefeld/ftp.era.ebi.ac.uk/vol1/fastq/SRR592/008/SRR5925338/SRR5925338_2.fastq.gz'
    'bielefeld/ftp.era.ebi.ac.uk/vol1/fastq/SRR592/002/SRR5925352/SRR5925352_1.fastq.gz'
    'bielefeld/ftp.era.ebi.ac.uk/vol1/fastq/SRR592/002/SRR5925352/SRR5925352_2.fastq.gz'
    'bielefeld/ftp.era.ebi.ac.uk/vol1/fastq/SRR592/005/SRR5925355/SRR5925355_1.fastq.gz'
    'bielefeld/ftp.era.ebi.ac.uk/vol1/fastq/SRR592/005/SRR5925355/SRR5925355_2.fastq.gz'
)

mc -q cp ${urls[${number}]} /vol/scratch/$folder_date/$number/
mc -q cp ${urls[${number} + 1]} /vol/scratch/$folder_date/$number/


# Run kraken2 on this fastq-pair and output the result to the NFS-Share on /vol/spool/
kraken2 --db /vol/scratch/kraken2db/ --paired --threads 24 /vol/scratch/$folder_date/$number/*.fastq.gz > /vol/scratch/$folder_date/$number/result.txt

# Some postprocessing which is needed for krona...
cat /vol/scratch/$folder_date/$number/result.txt | cut -f 2,3 > /vol/spool/krona_results/result_${SLURM_ARRAY_TASK_ID}.krona

# Remove the preprocessed file
rm -rf /vol/scratch/$folder_date/